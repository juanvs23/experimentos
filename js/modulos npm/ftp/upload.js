'use strict'


var http=require('http').createServer(serverOn),
	util=require('util'),
	formidable=require('formidable'),
	fse=require('fs-extra'),
	port=3000,
    gateWay="127.0.0.1";

	
function serverOn (req, res) 
{
	//si hay requerimiento y el metodo es post
	  if (req.url == '/upload' && req.method.toLowerCase() == 'post') 
	  {
		    	// parse a file upload 
		    	var form = new formidable.IncomingForm();
				    form.parse(req, function(err, fields, files) {
				      		   res.writeHead(200, {'content-type': 'text/html'});
				     		   res.write(
				     		   	'<h1>received upload:\n\n</h1>' + 
				     		   	util.inspect({files:files}) +
				     		   	'<a href="/">Regresar</a>'
				     		   	);
				     		   res.end( );
		    });
				    //calcula la descarga
		form.on('progress',function (bytesReceived,bytesExpected) {
				var downloadComplete=(bytesReceived/bytesExpected)*100;
						console.log(downloadComplete.toFixed(2) + ' %');
			});
					//finaliza el envio 
		form.on('end', function (fields,files) {
				//nombre y direccion
				var tempPath= this.openedFiles[0].path,
					fileName= this.openedFiles[0].name,
					// nueva localizacion
					newLocation='./upload/' + fileName
						//copia el archivo en la ubicacion indicada
					fse.copy(tempPath, newLocation, function (err) {
						return (err) ? console.log(err) : console.log('upload complete');
					})
			}) 
		 
		    return;
  		}	

	res.writeHead(200, {'content-type': 'text/html'});
  res.end(
    '<form action="/upload" enctype="multipart/form-data" method="post">'+
    '<input type="file" name="upload" multiple="multiple"><br>'+
    '<input type="submit" value="Upload">'+
    '</form>'
  );
}
http.listen(port,gateWay);

console.log('this server run  gateWay: ' + gateWay + ' port: '+ port);