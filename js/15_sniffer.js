'use strict'

var http= require('http'),
	port=3001,
	gateWay="127.0.0.1",
	option={
		host:'localhost',
		port:'3000',
		path:'/'
	};

	function httpClient (res) {
		console.log(`El sitio ${option.host} ha respondido. codigo de estado ${res.statusCode}`);
		res.on('data', function (data) {
			htmlCode += data
			console.log(data);
		})
	}
	function httpError (err) {
		console.log(`El sitio ${option.host} NO ha respondido. codigo de error ${err.code}. ${err.message}`);
	}

	function serverOn(req, res) {
		res.writeHead(200, {'Content-Type':'text/html'})
		res.end(htmlCode)
}
http
	.get(option,httpClient)
	.on('error',httpError)

http.createServer(serverOn)
http.listen(port,gateWay)

console.log('this server run  gateWay: ' + gateWay + ' port: '+ port);