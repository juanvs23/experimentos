'use strict'

var entrada = process.stdin,
	salida =process.stdout,
	tes={
		name:null,
		age:0
	};
function saveAge(age) {
	tes.age=age
	if (tes.age>=18) {
		salida.write(tes.name +' tiene edad suficiente para beber con '+ tes.age + ' años\n')
	} else {
		salida.write(tes.name + ' es menor de edad y no puede beber con '  + tes.age + ' años\n')
	}
	process.exit()
}
function saveName(name) {
	tes.name=name
	var question = 'hola ' + tes.name + ' cual es tu edad?'
	quiz(question, saveAge)
}
function quiz(question,callback) {
	entrada.resume()
	salida.write(question + ':')
	entrada.once('data',function (res) {
		callback(res.toString().trim())
	})
}
entrada.setEncoding('utf-8');
quiz('hola tu nombre? ', saveName)