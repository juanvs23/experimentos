'use strict'
var http= require('http').createServer(serverOn),
	Path= require('path'),
	url=require('url'),
	fs=require('fs'),
    port=3000,
	gateWay="127.0.0.1",
    urls=[
    	{
    		id:1,
    		route:'',
    		output:'asset/read/index.html'
    	}, {
    		id:2,
    		route:'acerca',
    		output:'asset/read/acerca.html'
    	}, {
    		id:3,
    		route:'contacto',
    		output:'asset/read/contacto.html'
    	}
    ];


	function serverOn(req, res) {
		var pathUrl=Path.basename(req.url),
			id=url.parse(req.url, true).query.id;
			//console.log(pathUrl);
			console.log(`ruta: ${pathUrl} id:${id}`);

			urls.forEach(function(pos) {
				if (pos.route==pathUrl || pos.id == id) {
					res.writeHead(200, {'Content-Type':'text/html'})
					fs.readFile(pos.output,function readFile(err, data) {
				if(err) throw err 
				res.end(data)
			})
	
				} 			
			})
			if (!res.finished) {
				res.writeHead(404, {'Content-Type':'text/html'})
				fs.readFile('asset/read/404.html',function readFile(err, data) {
				if(err) throw err 
				res.end(data)
			})
	
			} 
}
http.listen(port,gateWay);

console.log('this server run  gateWay: ' + gateWay + ' port: '+ port);