'use strict'

var http=require('http').createServer(serverOn),
	fs=require('fs').readFileSync('asset/read/form.html'),
	querystring=require('querystring'),
	util=require('util'),
	port=3001,
	dataString='',
	gateWay="127.0.0.1";
	


function serverOn (req, res) {
	if (req.method=='GET') {
		res.writeHead(200, {'Content-Type':'text/html'})
		res.end(fs)
	} 

	if (req.method=='POST') {
		req
			.on('data', function (data) {
				if (data=="") {
					// statement
					dataString='No hay datos'
				} else {
					// statement
				dataString += data
				}
			})
	}	req.on('end',function () {
		var	dataObject=querystring.parse(dataString),
		    dataJSON=util.inspect(dataObject),
		    /*templateString=`
Los datos enviados como 'string' son: ${dataString}
		    `,//enviar como String
		    templateObject=`
Los datos enviados como objetos son: ${dataObject.name} y ${dataObject.email}
		    `,//enviar como objeto(probar for in)*/
		    templateJSON=`
Los datos enviados como objetos son: ${dataJSON}`//enviar como JSON(probar posibilidades... muchas)
		 console.log(templateJSON);
		 res.end(templateJSON)
	})
}

http.listen(port,gateWay)

console.log('this server run  gateWay: ' + gateWay + ' port: '+ port);