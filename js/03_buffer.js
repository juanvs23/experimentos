'use strict'
/*un buffer es una tira de bytes similar a un arreglos de numeros enteros
su tamaño es fijo
Dentro de un buffer podemos manejar sockets y streams
implemntar protocolos complejos
manipulacion de ficheros e imagenes
criptografia*/

var buf= new Buffer(100),
    buf2= new Buffer(26),
    str= '\u00bd + \u00bc = \u00be';
    
    console.log(buf);
    console.log(buf.write('abcd', 0, 4, 'ascii')); 

    console.log(
    	buf,
    	buf.toString('ascii'),
    	str,
    	str.length,
    	Buffer.byteLength(str,'utf-8'));

    for (var i=0; i< buf2.length; i++) {
    	// statement
    	 buf2[i]=i + 97;
    }
    	console.log(buf2.toString('utf-8'));
