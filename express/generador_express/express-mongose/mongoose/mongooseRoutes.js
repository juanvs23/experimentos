var express = require('express');
var router = express.Router();
var mongooseController = require('./mongooseController.js');

/*
 * GET
 */
router.get('/', mongooseController.list);

/*
 * GET
 */
router.get('/:id', mongooseController.show);

/*
 * POST
 */
router.post('/', mongooseController.create);

/*
 * PUT
 */
router.put('/:id', mongooseController.update);

/*
 * DELETE
 */
router.delete('/:id', mongooseController.remove);

module.exports = router;
