var mongooseModel = require('./mongooseModel.js');

/**
 * mongooseController.js
 *
 * @description :: Server-side logic for managing mongooses.
 */
module.exports = {

    /**
     * mongooseController.list()
     */
    list: function (req, res) {
        mongooseModel.find(function (err, mongooses) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting mongoose.',
                    error: err
                });
            }
            return res.json(mongooses);
        });
    },

    /**
     * mongooseController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        mongooseModel.findOne({_id: id}, function (err, mongoose) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting mongoose.',
                    error: err
                });
            }
            if (!mongoose) {
                return res.status(404).json({
                    message: 'No such mongoose'
                });
            }
            return res.json(mongoose);
        });
    },

    /**
     * mongooseController.create()
     */
    create: function (req, res) {
        var mongoose = new mongooseModel({

        });

        mongoose.save(function (err, mongoose) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating mongoose',
                    error: err
                });
            }
            return res.status(201).json(mongoose);
        });
    },

    /**
     * mongooseController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        mongooseModel.findOne({_id: id}, function (err, mongoose) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting mongoose',
                    error: err
                });
            }
            if (!mongoose) {
                return res.status(404).json({
                    message: 'No such mongoose'
                });
            }

            
            mongoose.save(function (err, mongoose) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating mongoose.',
                        error: err
                    });
                }

                return res.json(mongoose);
            });
        });
    },

    /**
     * mongooseController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        mongooseModel.findByIdAndRemove(id, function (err, mongoose) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the mongoose.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
