'use strict'
var express = require('express'),
	/*cookie=require('cookie'),*/
	cookieParser = require('cookie-parser'),
	cookieSession = require('cookie-session'),
	  envio = express(),
	  port  = 3000,
	  gateWay ="127.0.0.1",
	  domain_Url ='/';

/*  envio.use( cookie )*/
  envio.use( cookieSession({name:"juan",keys: ['key1', 'key2']}) )
  envio.use( cookieParser() )

envio.get( domain_Url, (req, res, next)=>{
	

	req.session.visitas || (req.session.visitas =0)//este metodo es similar a if
	var nro = req.session.visitas++
	

	/*var nro = 0;
	//metodo largo
	if (req.session.visitas) {
		nro = req.session.visitas++
	}else {
		req.session.visitas=0
	}*/

	res.send(`
				<h1 style="text-align:center; font-style:arial; margin-top:60px;">
					Hola mundo desde EXPRESS
				</h1>
				<p style= "text-align:center; font-style:helvetica; margin-top:20px; font-size:30px">
					Esta es tu visitas <b style="color:red;"> ${nro}</b>
				</p>
			`)

});

envio.listen(port,()=>{console.log(`iniciando en el ${port}`)});