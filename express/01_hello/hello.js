'use strict'
var express = require('express');//llama al modulo express
var hello = express();//instanciamos la funcion express en una variable

hello.get('/', (req, res)=> { 
  res.send(`<h1 style="font-size:36px;color:blue;position:absolute;top:50%;left:30%;">HOLA JEFE</h1>`);
});//funcion para crear el servidor funcion.get('ruta',funcion anonima(request, response){ x cantidad de funciones callback, en este caso la funcion send y a traves de ella enviamos nuestro clasico 'hello world'})

hello.listen(3000, function () {
  console.log('Example app listening on port 3000!' );
});//funcion de escucha (puerto, funcion anonima)

