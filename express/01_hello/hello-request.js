'use strict'

var express = require('express'),
	  envio = express(),
	  port  = 3000,
	  gateWay ="127.0.0.1",
	  domain_Url_index ='/',
	  domain_Url_search ='/search/:user-:clave-:ano',
	  domain_Url_test ='/:test',
	  domain_Url_redirec ='/:test';

/*enviar requerimientos mediente el metodo GET, utilizando parametros*/


envio.get( domain_Url_index, ( req , res )=>{
	res.sendFile(__dirname + '/asset/404.html')
});//se lee un archivo y a la vez se sube al servidor

envio.get('/whoop', (req, res)=>{
	res.send(new Buffer('whoop'))
});//enviar un buffer

envio.get('/redirec', (req, res)=>{
	res.redirect(301, 'https://www.google.co.ve/')
});

envio.get('/404', (req, res)=>{
	res.status(404).send('Sorry, we cannot find that!');
});//envio de estatus de conexion

envio.get('/json', (req, res)=>{
	res.send({user:'juan', edad: '36 años'});
});//envio paquetes json

envio.get('/json', (req, res)=>{
	res.json({user:'juan', edad: '36 años'});
});//envio paquetes json

envio.get( domain_Url_search , ( req , res )=>{
	res.send(`<h1>Hola estoy operando en un servidor hecho en javaScript</h1>
		<p>Esto gracias a NodeJs y el parametro enviado por GET es: ${req.params.user}... tu clave es: ${req.params.clave}</p>
		`);
});//se envia los datos al servidor y se envian los parametros mediante la URL

envio.get( domain_Url_test, ( req, res )=>{
	res.send(`
				<h1>Trabajo realizado con express y node en modo de prueba</h1> <br />
				<p>el resultado es: ${req.query.user}</p>
		`)
})//con query podemos indicarle un parametro de busqueda que funcionara del siquiente modo "/?parametro=elemento a buscar"


envio.listen(port,()=>{console.log(`iniciando en el ${port}`)});